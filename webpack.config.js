"use strict";

/**
* Webpack Config
*/
const path = require("path");
const fs = require("fs");
const webpack = require("webpack");
// const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

// Webpack uses `publicPath` to determine where the app is being served from.
// In development, we always serve from the root. This makes config easier.
const publicPath = "/";

// Make sure any symlinks in the project folder are resolved:
const appDirectory = fs.realpathSync(process.cwd());
const resolveApp = relativePath => path.resolve(appDirectory, relativePath);

// plugins
const HtmlWebPackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CleanWebpackPlugin = require("clean-webpack-plugin");
const TerserPlugin = require("terser-webpack-plugin");

let config = {
//entry: ["babel-polyfill", "./src/index.js"],
node: { fs: "empty" },
output: {
    // The build folder.
    path: resolveApp("dist"),
    // Generated JS file names (with nested folders).
    // There will be one main bundle, and one file per asynchronous chunk.
    // We don't currently advertise code splitting but Webpack supports it.
    filename: "[name].[hash].js",
    chunkFilename: "[name].[hash].chunk.js",
    // We inferred the "public path" (such as / or /my-project) from homepage.
    publicPath: publicPath,
},
devServer: {
    contentBase: "./src/index.js",
    compress: true,
    port: 3000, // port number
    historyApiFallback: true,
},
optimization: {
    minimizer: [
    new TerserPlugin({
        sourceMap: process.env.NODE_ENV === "production",
        terserOptions: {
        mangle: true,
        toplevel: true,
        module: true,
        },
    }),
    ],
},
// resolve alias (Absolute paths)
resolve: {
    modules: [
    path.resolve(__dirname, "src"),
    path.resolve(__dirname, "node_modules"),
    ],
},
module: {
    rules: [
    {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: {
        loader: "babel-loader",
        },
    },
    {
        test: /\.html$/,
        use: [
        {
            loader: "html-loader",
            options: { minimize: true },
        },
        ],
    },
    {
        test: /\.css$/,
        use: [MiniCssExtractPlugin.loader, "css-loader"],
    },
    {
        test: /\.(xlsx)$/,
        use: [
        {
            loader: "file-loader",
            options: {
            limit: 10000,
            name: "static/media/[name].[hash:8].[ext]",
            },
        },
        ],
    },
    {
        test: /\.(png|jpg|gif|pdf)$/,
        use: [
        {
            loader: "url-loader",
            options: {
            limit: 10000,
            name: "static/media/[name].[hash:8].[ext]",
            },
        },
        ],
    },
    {
        test: /\.po$/,
        use: ["json-loader", "po-gettext-loader"],
    },
    {
        test: /\.(woff|woff2|eot|ttf|svg)$/,
        loader: "url-loader?limit=100000",
    },
    // Scss compiler
    {
        test: /\.scss$/,
        use: [
        {
            loader: "style-loader",
        },
        {
            loader: "css-loader",
        },
        {
            loader: "sass-loader",
        },
        ],
    },
    ],
},
performance: {
    hints: process.env.NODE_ENV === "production" ? "warning" : false,
},
plugins: [
    new webpack.DefinePlugin({
    "process.env.NODE_ENV": JSON.stringify(process.env.NODE_ENV),
    "process.env.BASE_URL": JSON.stringify(process.env.BASE_URL)
    }),

    new HtmlWebPackPlugin({
    template: "./public/index.html",
    filename: "./index.html",
    favicon: "./public/favicon.ico",
    }),
    new MiniCssExtractPlugin({
    filename: "[name].css",
    chunkFilename: "static/css/[name].[hash:8].css",
    }),
],
};

config.devtool = "hidden-source-map";
if (process.env.NODE_ENV !== "production") {
config.devtool = "inline-source-map";
}

module.exports = config;