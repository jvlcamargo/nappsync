import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import FormRegister from '../Form/register'

export default function AlertDialog() {
  const [open,
    setOpen] = React.useState(false);

  function handleClickOpen() {
    setOpen(true);
  }

  function handleClose() {
    setOpen(false);
  }

  return (
    <div>
      <Button variant="outlined" color="inherit" onClick={handleClickOpen}>
        New Server
      </Button>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description">
        <DialogTitle id="alert-dialog-title">{"Create New Server"}</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            <FormRegister/>
          </DialogContentText>
        </DialogContent>
      </Dialog>
    </div>
  );
}