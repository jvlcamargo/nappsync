export const style = {
  container: {
    display: 'flex',
    flexWrap: 'wrap',
    width: '100%'
  },
  input: {
    width: '100%',
    marginBottom: 10
  }
};
