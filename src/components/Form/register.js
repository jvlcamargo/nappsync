import React from 'react';
import Button from '@material-ui/core/Button';
import {ValidatorForm, TextValidator} from 'react-material-ui-form-validator';
import {style} from './style';
import Input from '@material-ui/core/Input';
import FormHelperText from '@material-ui/core/FormHelperText';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Api from '../../services/api';
import SnackBarSuccess from '../SnackBarSuccess';
import SnackBarFail from '../SnackBarFail';
import CircularProgress from '../CircularProgress'

export default class Register extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      formData: {
        name: undefined,
        host: undefined
      },
      isLoading: false,
      selected: null,
      hasError: false,
      categories: [],
      categoryId: undefined,
      statusSubmit: null
    }
  }
  
  componentDidMount() {
    this.loadCategories();
  }

  loadCategories = async() => {
    const response = await Api.get('category-leafs');
    const categories = response.data; 
    this.setState({categories: categories});
  };

  handleChangeSelect(e) {
    this.setState({
      selected: e.target.value, categoryId: e.target.value, hasError: false})
  }
  
  //otimizar estas duas funções
  handleChange = (event) => {
    const {formData} = this.state;
    formData[event.target.name] = event.target.value;
    this.setState({formData});
  }

  handleClick() {
    if (!this.state.selected) {
      this.setState({hasError: true});
    }
  }
  handleSubmit = async() => {
    if (!this.state.hasError) {
      this.setState({isLoading: true})
      await Api
        .post('/servers/', {
        name: this.state.formData.name,
        host: this.state.formData.host,
        category: this.state.categoryId
      })
        .then(() => {
          this.setState({
            isLoading: false,
            statusSubmit: true
          }, () => {
            setTimeout(() => this.setState({statusSubmit: null}), 3000);
          });
        })
        .catch((error) => {
          this.setState({
            isLoading: false,
            statusSubmit: true
          }, () => {
            setTimeout(() => this.setState({statusSubmit: null}), 3000);
          });
          console.log(error.status);
        });
    }
  }

  render() {
    const {formData, isLoading, categories, statusSubmit, selected, hasError} = this.state;
    return (
      <div style={style.container}>
        <ValidatorForm onSubmit={this
          .handleSubmit
          .bind(this)}>
          <TextValidator
            label="Name"
            onChange={this.handleChange}
            name="name"
            type="text"
            style={style.input}
            validators={['required']}
            errorMessages={['this field is required']}
            value={formData.name}/>
          <br/>
          <TextValidator
            label="Host"
            onChange={this.handleChange}
            name="host"
            type="text"
            value={formData.host}
            style={style.input}
            validators={['required']}
            errorMessages={['this field is required']}/>
          <FormControl error={hasError} fullWidth>
            <InputLabel htmlFor="category">Category</InputLabel>
            <Select
              name="category"
              style={style.input}
              value={selected}
              onChange={this
              .handleChangeSelect
              .bind(this)}
              input={< Input id = "category" />}>
              {categories.map(category => (
                <MenuItem key={category.id} value={category.id}>{category.name}</MenuItem>
              ))}
            </Select>
            {hasError && <FormHelperText>This field is required!</FormHelperText>}
          </FormControl>
          <br/> 
          
          {
            statusSubmit === true ? <SnackBarSuccess/>
            : statusSubmit === false ? <SnackBarFail/>
            : undefined
          }

          <Button type="submit" color="primary" onClick={() => this.handleClick()}>
            {(isLoading
              ? <CircularProgress/>
              : 'Submit')}
          </Button>

        </ValidatorForm>
      </div>
    );
  }
}