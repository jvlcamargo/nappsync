import React, {Component} from 'react';
import MenuItem from '@material-ui/core/MenuItem';
import {TextValidator, SelectValidator} from 'react-material-ui-form-validator';
import {style} from './style';
import Api from '../../services/api'
import Input from '@material-ui/core/Input';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';

class Form extends Component {

  state = {
    categories: []
  };

  componentDidMount() {
    this.loadCategories();
  }
  loadCategories = async() => {
    const response = await Api.get('category-leafs/');
    const categories = response.data; 
    this.setState({categories: categories});
  };

  handleChange = (event) => {
    this.setState({ [event.target.name]: event.target.value });
  }

  render() {
    const {name, host, port, category, categoryId} = this.props.state;
    return (
      <div style={style.container}>
        <TextValidator
          placeholder="Type the store name"
          style={style.input}
          inputProps={{
          'aria-label': 'Server Name'
        }}
          label="Server Name"
          onChange={this.props.handleChange}
          name="name"
          value={name}
          type="text"
          validators={['required']}
          title="Field for entering the server name"
          errorMessages={['this field is required']}/>

        <TextValidator
          placeholder="192.168.50.88"
          style={style.input}
          value={host}
          disabled={this.props.edit}
          inputProps={{
          'aria-label': 'Server Host'
        }}
          label="Server Host"
          onChange={this.props.handleChange}
          name="host"
          validators={['required']}
          title="Field for entering the Server Host"
          errorMessages={['this field is required']}/>

        <TextValidator
          placeholder="3333"
          style={style.input}
          inputProps={{
          'aria-label': 'Port'
        }}
          disabled={this.props.edit}
          label="Server Port"
          onChange={this.props.handleChange}
          name="port"
          value={port}
          validators={['required', 'matchRegexp:^[0-9]*$']}
          title="Server port"
          errorMessages={['this field is required', 'port must have only numbers']}/>
  
        <FormControl fullWidth>
          <InputLabel htmlFor={category.name}>{category.name}</InputLabel>
          <SelectValidator
            label={category.name}
            style={style.input}
            onChange={this.handleChange}
            name="category"
            value={category}
            validators={['required']}
            title="Field for alter the category"
            input={< Input id = {category.id} />}
            errorMessages={['this field is required']}>
            {this
              .state
              .categories
              .map(category => (
                <MenuItem key={category.id} value={category.id}>{category.name}</MenuItem>
              ))}
          </SelectValidator>
        </FormControl>
      </div>
    );
  }
}

export default Form;