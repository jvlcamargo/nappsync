import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import {styles} from './styles';
import DialogRegister from '../../components/DialogRegister'
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import {onSignOut} from '../../services/auth'

export default class ButtonAppBar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      port: '',
      host: '',
      category: ''
    }
  }


  logout = () => {
    onSignOut();
    window.location.href = "/login"
  }

  render() {
    return (
      <div className={styles.root}>
        <AppBar position="static">
          <Box display="flex">
            <Toolbar >
              <Typography variant="h6" style={styles.marginRight} justify="space-between">
                Napp Sync
              </Typography>
              <Button color="inherit" onClick={this.logout} >Logout</Button>
              <Box flexDirection="row-reverse">
                <DialogRegister/>
              </Box>
            </Toolbar>
          </Box>
        </AppBar>
      </div>
    )
  }
}