export const styles = {
  root: {
    flexGrow: 1
  },
  menuButton: {
    margin: 12
  },
  title: {
    flexGrow: 1
  },
  marginRight: {
    marginRight: 150
  }
};
