import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Form from '../Form';
import {ValidatorForm} from 'react-material-ui-form-validator';
import CircularProgress from '../CircularProgress';
import Api from '../../services/api';
import SnackBarSuccess from '../SnackBarSuccess';
import SnackBarFail from '../SnackBarFail';

export default class DialogEditServer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      id: undefined,
      host: undefined,
      name: undefined,
      port: undefined,
      category: undefined,
      isLoading: false,
      statusSubmit: null,
      categories: [],
      categoryId: ''
    }
  }

  UNSAFE_componentWillReceiveProps(nextProps, b) {
    this.setState({
      ...nextProps.data
    })
  }

  handleSubmit = async() => {
    this.setState({isLoading: true});
    await Api
      .patch(`/servers/${this.state.id}/`, {
      name: this.state.name,
      category: this.state.categoryId
        ? this.state.categoryId
        : this.state.category.id
    })
      .then(() => {
        this.setState({
          isLoading: false,
          statusSubmit: true
        }, () => {
          setTimeout(() => this.setState({statusSubmit: null}), 3000);
        });
      })
      .catch((error) => {
        this.setState({
          isLoading: false,
          statusSubmit: false
        }, () => {
          setTimeout(() => this.setState({statusSubmit: null}), 3000);
        });
      });
  }

  handleChange = (event) => {
    this.setState({
      ...this.state,
      [event.target.name]: event.target.value
    })
  }

  handleChangeSelect(e) {
    this.setState({categoryId: e.target.value})
  }
  render() {
    return (
      <div>
        <Dialog
          open={this.props.open}
          onClose={this.props.onClose}
          aria-labelledby="draggable-dialog-title">
          <ValidatorForm
            ref="form"
            instantValidate={true}
            onSubmit={this.handleSubmit}
            onError={errors => console.log(errors)}>
            <DialogTitle>
              Edit Server {this.props.categoryId}{this.props.categoryId}
            </DialogTitle>
            <DialogContent>
              <DialogContentText>
                <Form
                  edit={Boolean(this.props.data)}
                  state={this.state}
                  handleChange={this
                  .handleChange
                  .bind(this)}
                  handleChangeSelect={this
                  .handleChangeSelect
                  .bind(this)}/>
              </DialogContentText>
            </DialogContent>
            <DialogActions>
              <Button onClick={this.props.onClose} color="primary">
                Cancel
              </Button>
              <Button type="submit" color="inherit" onClick={this.handleChange}>
                {(this.state.isLoading
                  ? <CircularProgress/>
                  : 'Save')}
              </Button>
              {this.state.statusSubmit === true
                ? <SnackBarSuccess/>
                : this.state.statusSubmit === false
                  ? <SnackBarFail/>
                  : undefined
}
            </DialogActions>
          </ValidatorForm>
        </Dialog>
      </div>
    );
  }
}