import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import {blue} from '@material-ui/core/colors';
import Avatar from '@material-ui/core/Avatar';
import SyncIcon from '@material-ui/icons/Sync';
import Grid from '@material-ui/core/Grid';

const useStyles = makeStyles({
  avatar: {
    margin: 10
  },
  blueAvatar: {
    margin: 10,
    color: '#fff',
    backgroundColor: blue[900]
  }
});

export default function LoginIconAvatar() {
  const classes = useStyles();

  return (
    <Grid container justify="center" alignItems="center">
      <Avatar className={classes.blueAvatar}>
        <SyncIcon/>
      </Avatar>
    </Grid>
  );
}