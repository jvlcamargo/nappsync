import React from 'react';
import Button from '@material-ui/core/Button';

export default class ButtonRegister extends React.Component {
  render() {
    return (
      <React.Fragment>
        <Button type="submit" color="inherit">
          {(isLoading
            ? <CircularProgress/>
            : 'Save')}
        </Button>
      </React.Fragment>
    )
  }
}