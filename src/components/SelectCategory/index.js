import React from 'react';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';

export default function ControlledOpenSelect() {

const [age, setAge] = React.useState('');
const [open, setOpen] = React.useState(false);

function handleChange(event) {
    setAge(event.target.value);
}

function handleClose() {
    setOpen(false);
}

function handleOpen() {
    setOpen(true);
}

return (
    <FormControl fullWidth>
        <InputLabel htmlFor="demo-controlled-open-select">Categoria selecionada</InputLabel>
        <Select
        open={open}
        onClose={handleClose}
        fullWidth
        onOpen={handleOpen}
        value={age}
        onChange={handleChange}
        inputProps={{
            name: 'name',
            id: 'demo-controlled-open-select',
        }}
        >
        <MenuItem value="">
            <em>test</em>
        </MenuItem>
        <MenuItem value={20}>Ten</MenuItem>
        <MenuItem value={20}>Twenty</MenuItem>
        <MenuItem value={30}>Thirty</MenuItem>
        </Select>
    </FormControl>
);
}