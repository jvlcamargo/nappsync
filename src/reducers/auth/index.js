const initialState = {
    user: {},
    token: "",
    isLoading: false,
    errors: []
};

export const Auth = (state = initialState, action) => {
    switch (action.type) {
        case 'AUTH_REQUEST':
            return {
                ...state,
                isLoading: true
            };
        case 'AUTH_FULFILL':
            return {
                ...state,
                user: action.payload.user,
                token: action.payload.token,
                isLoading: false
            };
        default:
            return state;
    }
}