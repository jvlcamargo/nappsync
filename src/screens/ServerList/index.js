import MUIDataTable from "mui-datatables";
import React from "react";
import Dialog from '../../components/Dialog';
import Fab from '@material-ui/core/Fab';
import EditIcon from '@material-ui/icons/Edit';
import CallMade from '@material-ui/icons/CallMade';
import CloudQueue from '@material-ui/icons/CloudQueue';
import CloudOff from '@material-ui/icons/CloudOff';
import api from '../../services/api';
import ProgressBar from '../../components/ProgressBar';
import {style} from "./style";
import GetAppIcon from '@material-ui/icons/GetApp';
import {isSignedIn} from '../../services/auth'

class ServerList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      dialogOpen: false,
      selectedServer: {},
      serversList: [],
      loading: true
    }
  }
  toggleDialog = () => {
    this.setState({
      dialogOpen: !this.state.dialogOpen
    })
  }

  setData = (data) => {
    this.toggleDialog();
    this.setState({selectedServer: data})
  }

  async loadServers() {
    let data = await api
      .get('/servers/')
      .then((response) => {
        return response.data
      });
    return data
  };

  async componentDidMount() {
    !isSignedIn() && (window.location.href = "/login")

    let serversList = await this.loadServers();

    if (serversList) {
      this.setState({serversList, loading: false})
    }
  }
  render() {
    let data = this
      .state
      .serversList
      .map((i) => {
        if (i) {
          return [
            i.name,
            i.host,
            i.port,
            i.category.name,
            i.is_online,
            i,
            i,
            i
          ]
        }
      })
    const columns = [
      "Name",
      "Host",
      "Port",
      "Category", {
        name: "Online",
        options: {
          filter: true,
          sort: false,
          customBodyRender: (value, tableMeta, updateValue) => {
            return value
              ? <CloudQueue style={{
                  color: "green"
                }}/>
              : <CloudOff style={{
                color: "red"
              }}/>
          }
        }
      }, {
        name: "Edit",
        options: {
          filter: true,
          sort: false,
          customBodyRender: (value, tableMeta, updateValue) => {
            return (
              <Fab
                onClick={() => {
                this.setData(value)
              }}
                color="primary"
                size="small"
                aria-label="edit"
                style={style.fab,
              style.extendedIcon}>
                <EditIcon/>
              </Fab>
            );
          }
        }
      }, {
        name: ".bat",
        options: {
          filter: true,
          sort: false,
          customBodyRender: (i) => {
            return (
              <Fab
                href={`${process.env.BASE_URL}servers/${i.id}/batch`}
                download
                color="default"
                aria-label="access"
                size="small"
                style={style.fab,
              style.extendedIcon}>
                <GetAppIcon/>
              </Fab>
            )
          }
        }
      }, {
        name: "Access",
        options: {
          filter: true,
          sort: false,
          customBodyRender: (i) => {
            return (
              <Fab
                href={`http://${i.host}:${i.port}`}
                target="_blank"
                color="default"
                aria-label="access"
                size="small"
                style={style.fab,
              style.extendedIcon}>
                <CallMade/>
              </Fab>
            )

          }
        }
      }
    ];

    const options = {
      filterType: 'checkbox'
    };
    return (
      <React.Fragment>
        {this.state.loading
          ? <ProgressBar/>
          : undefined}
        <MUIDataTable
          title={"Servers List"}
          data={data}
          columns={columns}
          options={options}/>
        <Dialog
          data={this.state.selectedServer}
          open={this.state.dialogOpen}
          state={this.state}
          onClose={this
          .toggleDialog
          .bind(this)}/>
      </React.Fragment>
    );
  }
}
export default ServerList;
