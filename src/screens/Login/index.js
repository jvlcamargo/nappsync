import React from 'react';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Paper from '@material-ui/core/Paper';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import {withStyles} from '@material-ui/core/styles';
import Copyright from './Copyright';
import {styles} from './styles';
import LoginAvatar from '../../components/LoginAvatar'
import SnackBarFail from '../../components/SnackBarFail'
import api from '../../services/api';
import {isSignedIn, setToken} from '../../services/auth'

class SignIn extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      statusSubmit: undefined,
      isLoading: undefined,
      email: '',
      password: ''
    }
  }
  componentDidMount() {
    isSignedIn() && (window.location.href = "/")
  }

  handleSubmit = async(event) => {
    event.preventDefault();
    this.setState({isLoading: true});
    const {email, password} = this.state;
    await api
      .post("auth/jwt/create", {
      email: email,
      password: password
    })
      .then((response) => {
        this.setState({isLoading: false, statusSubmit: true});
        setToken(response);
        window.location.href = "/"
      })
      .catch((error) => {
        this.setState({
          isLoading: false,
          statusSubmit: false
        }, () => {
          setTimeout(() => this.setState({statusSubmit: null}), 3000);
        });

      });
  }

  handleChange = (event) => {
    this.setState({
      ...this.state,
      [event.target.name]: event.target.value
    })
  }

  render() {
    const {classes} = this.props;
    return (
      <Grid container component="main" className={classes.root}>
        <CssBaseline/>
        <Grid item xs={false} sm={4} md={7} className={classes.image}/>
        <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
          <div className={classes.paper}>
            <LoginAvatar/>
            <Typography component="h1" variant="h5">
              Napp Sync
            </Typography>
            <form className={classes.form} onSubmit={this.handleSubmit}>
              <TextField
                onChange={this.handleChange}
                value={this.state.email}
                variant="outlined"
                margin="normal"
                required
                type="email"
                name="email"
                fullWidth
                id="email"
                label="Email Address"
                autoComplete="email"
                autoFocus/>
              <TextField
                onChange={this.handleChange}
                value={this.state.password}
                variant="outlined"
                margin="normal"
                required
                fullWidth
                name="password"
                label="Password"
                type="password"
                id="password"
                autoComplete="current-password"/>
              <Button
                fullWidth
                type="submit"
                variant="contained"
                color="primary"
                className={classes.submit}>
                {(this.state.isLoading
                  ? <CircularProgress/>
                  : 'Sing In')}
              </Button>
              <Grid container></Grid>
              <Box mt={5}>
                <Copyright/>
              </Box>
            </form>

            {this.state.statusSubmit === false
              ? <SnackBarFail/>
              : undefined
}
          </div>
        </Grid>
      </Grid>

    );

  }
}
export default withStyles(styles)(SignIn);
