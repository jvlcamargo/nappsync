export const loginRequest = () => ({type: 'AUTH_REQUEST'});

export const loginFulfill = ({data: user, token}) => ({
  type: 'AUTH_FULFILL',
  payload: {
    user,
    token
  }
});

export const login = (data) => {
  return (dispatch) => {
    dispatch(loginRequest())
  }
}