import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import configureStore from './store/configureStore';
import ServerList from './screens/ServerList';
import Header from './components/Header';
import './styles.css';
import Login from './screens/Login';
import {Switch, Route, Redirect, BrowserRouter as Router} from 'react-router-dom'
import {ThemeProvider} from '@material-ui/styles';
import { createMuiTheme } from '@material-ui/core/styles';;
import {isSignedIn, setConf} from './services/auth';

const store = configureStore();
const logged = isSignedIn();

logged && setConf();
 
const PrivateRoute = ({
  component: Component,
  ...rest
}) => (
  <Route
    {...rest}
    render={(props) => (logged
    ? <Component {...props}/>
    : <Redirect to='/login'/>)}/>
)
const theme = createMuiTheme({
  
});
ReactDOM.render(
  <ThemeProvider theme={theme}>
  <Provider store={store}>
    <Router>
      {logged && <Header/>}
      <Switch>
        <Route path="/login" component={Login}/>
        <PrivateRoute path="/" component={ServerList}/>
      </Switch>
    </Router>
  </Provider>
</ThemeProvider>, document.getElementById('root'));
