import api from '../services/api';

export async function VerifyToken(token) {

    let access = JSON.parse(token).access;
    
    await api
        .post("auth/jwt/verify", {token: access})
        .then(() => {
            return true;
        })
        .catch(() => {
            
            return false;
        });
}
