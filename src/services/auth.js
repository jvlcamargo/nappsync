import api from '../services/api';
import {VerifyToken} from './VerifyToken';

export const TOKEN_KEY = localStorage.getItem("token");

export function setConf() {

  api
    .interceptors
    .request
    .use(function (config) {
      const token = JSON
        .parse(TOKEN_KEY)
        .access;
      config.headers.Authorization = `JWT ${token}`;
      return config;
    })
}
export function setToken(response) {
  localStorage.setItem("token", JSON.stringify(response.data));
  api
    .interceptors
    .request
    .use(function (config) {
      const token = response.data.access;
      config.headers.Authorization = token;
      return config;
    });
}

export const onSignOut = () => localStorage.clear();

export function isSignedIn() {
  if (TOKEN_KEY) {
    if (VerifyToken(TOKEN_KEY)) {
      return true;
    } else {
      localStorage.clear()
      return false
    }
  } else {
    return false
  }
};