FROM node:10.16 as builder

ENV BASE_URL https://nappsync-backend.nappsolutions.com

COPY . /app

RUN useradd -m app && chown -R app:app /app

USER app

RUN cd /app && \
    npm ci -d && \
    npm run build

FROM nginx:1.15

COPY --from=builder /app/dist /usr/share/nginx/html